import datetime
import enum

from utils.errors import InvalidLogLV, InvalidLogPlatform


class DBLogger:
    class LogCategory(enum.Enum):
        DEBUG = 0
        LOW = 0
        INFO = 1
        MEDIUM = 1
        WARNING = 2
        HIGH = 2
        ERROR = 3

    class Platforms(enum.Enum):
        BACKEND = "BackEnd"
        APPLICATION = "Application"
        DISCORD = "Discord"
        TELEGRAM = "Telegram"
        YOUTUBE = "Youtube"
        TWITCH = "Twitch"
        API = "Api"

    def __init__(self, log_lv=0):
        from database import APPDATA

        self.log_lv = self.try_log_lv(log_lv)
        self.db = APPDATA

    def log(self, text, category=LogCategory.DEBUG, platform=Platforms.BACKEND):
        """
        saves a text with given parameters in the database for later examination
        If you are unsure what Strings or Ints are valid parameters, use the build in enums.

        Args:
            text: the message
            category: int from 0 to 3 (default = 0)
            platform: String with Platform name (default = BackEnd)
        """
        from database.db_models import Log

        category = self.try_log_lv(category)
        if category >= self.log_lv:
            platform = self.try_platform(platform)
            log_statement = Log(date=datetime.datetime.now(), platform=platform, category=category, message=text)
            self.db.db_session.add(log_statement)
            self.db.db_session.commit()

    def try_log_lv(self, x):
        if isinstance(x, self.LogCategory):
            return x.value
        if isinstance(x, int):
            if 4 > x >= 0:
                return x
        # if here not valid input_
        raise InvalidLogLV(f"The LogLV {x} does not exist")

    def try_platform(self, x):
        if isinstance(x, self.Platforms):
            return x.value
        if isinstance(x, str):
            try:
                return self.Platforms[x.upper()].value
            except KeyError:
                pass

        # if here not valid input_
        raise InvalidLogPlatform(f"The Platform {x} does not exist")

    def getLogs(self):
        from database.db_models import Log
        return Log.query().all()
