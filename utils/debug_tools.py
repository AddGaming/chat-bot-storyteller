"""
Functions to help with debugging
"""

import os


def dump_html(html, filename="trace"):
    """
    writes a Html string into a file. The file will be located in the test-cash

    Args:
        html: str - HTML as string
        filename: str - the filename of the new file (no .html needed)

    Returns: None
    """
    with open(f"{os.sep.join(os.getcwd().split(os.sep)[:-1])}{os.sep}{filename}.html", "w") as f:
        for row in html.content.decode("utf-8").split("\n"):
            f.write(row)
