"""
a collection of costume errors for the project
"""


class InvalidLogLV(ValueError):
    """
    An error that gets raised when you try to log at an unknown importance-Lv
    Supported are integer values from 0 to 3
    """
    pass


class InvalidLogPlatform(ValueError):
    """
    An error that gets raised when you try to log to a not registered/unknown platform
    """
    pass


class MissingArgument(ValueError):
    """
    An error that gets raised when you try to enwoke a function/method without providing enough arguments
    """
    pass


class UnsupportedOS(RuntimeError):
    """
    An error that gets raised when you run this on a not supported operating system.
    Currently supported are Windows and Linux.
    """
    pass


class InvalidArgumentConfiguration(ValueError):
    """
    An error that gets raised when you pass an invalid combination of args/kwargs into a function
    """
    pass


class ParameterRequired(Exception):
    """Raised when one optional value requires another, and one of them is not given"""
    pass


class IllegalArgument(Exception):
    """Raised when an Argument contains something that we dont allow (for example SQL-Commands as a Username"""
    pass
