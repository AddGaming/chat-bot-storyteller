"""
The WebInterface defines all the functionality relating to the web gui for the users.
In this ini file are also all configuration parameters.
"""
import datetime
import os

from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_session import Session

from utils.crypto import time_hash

DOMAIN = "127.0.0.1"
PORT = "5000"
PROTOCOL = "http"
# App initialisation
flask_app = Flask(__name__.split(".")[0])  # "complicated" to ensure a workable name is chosen
flask_app.static_folder = f"{os.getcwd()}{os.sep}web_interface{os.sep}static"
flask_app.template_folder = f"{os.getcwd()}{os.sep}web_interface{os.sep}templates"
flask_app.config['SECRET_KEY'] = time_hash() + time_hash() + time_hash()
flask_app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False  # disabling to suppress error
flask_app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{os.getcwd()}{os.sep}database{os.sep}userdata.db'
flask_app.config['PERMANENT_SESSION_LIFETIME'] = datetime.timedelta(hours=1)
_USER_DB = SQLAlchemy(flask_app)
flask_app.config['SESSION_TYPE'] = 'sqlalchemy'
flask_app.config['SESSION_SQLALCHEMY'] = _USER_DB
flask_app.config['SESSION_SQLALCHEMY_TABLE'] = "sessions"
_USER_DB.init_app(flask_app)
SESSION = Session(flask_app)


def create_app():
    """
    Returns: Flask-App
    """
    global _USER_DB
    global SESSION

    # database setup
    from database.db_models import User
    create_user_db(flask_app)

    # setting up routes
    from web_interface.routes.api_routing import api_routes
    from web_interface.routes.app_routing import app_routes
    from web_interface.routes.redirects import redirects

    flask_app.register_blueprint(api_routes, url_prefix="/api")
    flask_app.register_blueprint(app_routes, url_prefix="/app")
    flask_app.register_blueprint(redirects, url_prefix="/")

    # account management
    login_manager = LoginManager()
    login_manager.login_view = "app_routes.login"
    login_manager.init_app(flask_app)

    @login_manager.user_loader
    def load_user(u_id):
        """
        required function for LoginManager

        Args:
            u_id: user id

        Returns: user obj
        """
        from database import LOGGER

        # no validation required, since input_ only comes from login_manager
        loaded_user = User.query.get(int(u_id))
        LOGGER.log(
            f"Loaded user: {loaded_user.id}",
            category=LOGGER.LogCategory.DEBUG, platform=LOGGER.Platforms.BACKEND)
        return loaded_user

    return flask_app


def create_user_db(app):  # defining as function since it can't be executed here
    """
    Args:
        app: flask-app

    Returns: None
    """
    import importlib
    import sys
    from database.db_models import User, UserSettingProfile  # necessary for correct table creation

    global _USER_DB

    if not os.path.exists(f"{os.getcwd()}{os.sep}database{os.sep}userdata.db"):
        if os.getcwd().split(os.sep)[-1] != "chat-bot-storyteller":
            mod = sys.modules['database.db_models']
            importlib.reload(mod)
        print("no userdata table found, creating new...")
        _USER_DB.create_all(app=app)
