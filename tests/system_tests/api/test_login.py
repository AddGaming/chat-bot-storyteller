"""
Tests the functionality of the api
"""
import os
import unittest

import requests as rq

from utils.debug_tools import dump_html
from utils.decorators import create_test_env, load_package, boot_flask

DB_PATHS = [("one_admin_user.db", f"database{os.sep}userdata.db")]


class TestLogin(unittest.TestCase):

    @create_test_env(paths=DB_PATHS)
    @load_package(["database"])
    @boot_flask
    def test_normal_login(self):
        rq_data = {
            "mail": "test_mail",
            "pw": "password"
        }
        ans = rq.post("http://127.0.0.1:5000/api/login", data=rq_data)
        dump_html(ans, "test_normal_login")

        self.assertEqual(ans.status_code, 200)

    @create_test_env(paths=DB_PATHS)
    @load_package(["database"])
    @boot_flask
    def test_password_wrong(self):
        rq_data = {
            "mail": "test_mail",
            "pw": "password12"
        }
        ans = rq.post("http://127.0.0.1:5000/api/login", data=rq_data)
        dump_html(ans, "test_login_password_wrong")

        self.assertEqual(ans.status_code, 400)

    @create_test_env(paths=DB_PATHS)
    @load_package(["database"])
    @boot_flask
    def test_mail_illegal(self):
        rq_data = {
            "mail": "test_mail#",
            "pw": "password12"
        }
        ans = rq.post("http://127.0.0.1:5000/api/login", data=rq_data)
        dump_html(ans, "test_login_mail_illegal")

        self.assertEqual(ans.status_code, 400)

    @create_test_env(paths=DB_PATHS)
    @load_package(["database"])
    @boot_flask
    def test_password_illegal(self):
        rq_data = {
            "mail": "test_mail",
            "pw": "passw ord12"
        }
        ans = rq.post("http://127.0.0.1:5000/api/login", data=rq_data)
        dump_html(ans, "test_login_password_illegal")

        self.assertEqual(ans.status_code, 400)

    @create_test_env(paths=DB_PATHS)
    @load_package(["database"])
    @boot_flask
    def test_password_uppercase(self):
        rq_data = {
            "mail": "test_mail",
            "pw": "Password"
        }
        ans = rq.post("http://127.0.0.1:5000/api/login", data=rq_data)
        dump_html(ans, "test_login_password_uppercase")

        self.assertEqual(ans.status_code, 400)
