"""
Tests the functionality of the api
"""
import os
import unittest
import json

import requests as rq

from utils.decorators import create_test_env, load_package, boot_flask


class TestRegister(unittest.TestCase):

    @create_test_env(
        paths=[("one_admin_user.db", f"database{os.sep}userdata.db"),
               ("2_otts.db", f"database{os.sep}appdata.db")])
    @load_package(["database"])
    @boot_flask
    def test_ott_wo_admin(self):
        rq_data = {
            "mail": "normie_mail",
            "pw1": "password",
            "pw2": "password"
        }
        a = rq.post("http://127.0.0.1:5000/api/register", data=rq_data)
        ans = rq.post("http://127.0.0.1:5000/api/otts", cookies=a.cookies)

        self.assertEqual(ans.status_code, 403)

    @create_test_env(
        paths=[("one_admin_user.db", f"database{os.sep}userdata.db"),
               ("2_otts.db", f"database{os.sep}appdata.db")])
    @load_package(["database"])
    @boot_flask
    def test_ott_not_logged_in(self):
        ans = rq.post("http://127.0.0.1:5000/api/otts")

        self.assertEqual(ans.status_code, 403)

    @create_test_env(
        paths=[("one_admin_user.db", f"database{os.sep}userdata.db"),
               ("2_otts.db", f"database{os.sep}appdata.db")])
    @load_package(["database"])
    @boot_flask
    def test_ott_as_admin(self):
        rq_data = {
            "mail": "test_mail",
            "pw": "password"
        }
        a = rq.post("http://127.0.0.1:5000/api/login", data=rq_data)
        ans = rq.post("http://127.0.0.1:5000/api/otts", cookies=a.cookies)

        self.assertEqual(ans.status_code, 200)
        j = json.loads(ans.text)
        self.assertEqual(2, j["message"])

    @create_test_env(
        paths=[("one_admin_user.db", f"database{os.sep}userdata.db"),
               ("no_otts.db", f"database{os.sep}appdata.db"),
               ("nott.txt", f"database{os.sep}otts.txt")])
    @load_package(["database"])
    @boot_flask
    def test_no_ott_in_db(self):
        rq_data = {
            "mail": "test_mail",
            "pw": "password"
        }
        a = rq.post("http://127.0.0.1:5000/api/login", data=rq_data)
        ans = rq.post("http://127.0.0.1:5000/api/otts", cookies=a.cookies)

        self.assertEqual(ans.status_code, 200)
        j = json.loads(ans.text)
        self.assertEqual(0, j["message"])
