import os
import time
import unittest

from utils.decorators import create_test_env, load_package


# TODO: Optimize by using mockfiles that get coppied (txt and db)
class TestOttManagement(unittest.TestCase):

    @create_test_env()
    @load_package(["database"])
    def test_ott_creation_with_existing_file(self):
        # test for unique keys in each run
        from database import APPDATA
        APPDATA.generate_otts(2)
        time.sleep(5)
        with open(f"{os.getcwd()}{os.sep}database{os.sep}otts.txt", "r") as f:
            old_keys = f.readlines()
        APPDATA.generate_otts(2)
        time.sleep(5)  # waiting for keys to be encrypted and written
        with open(f"{os.getcwd()}{os.sep}database{os.sep}otts.txt", "r") as f:
            new_keys = f.readlines()
        self.assertNotEqual(new_keys[0], new_keys[1])
        for i, line in enumerate(new_keys):
            self.assertNotEqual(line, old_keys[i])

    @create_test_env()
    @load_package(["database"])
    def test_ott_creation_from_empty_env(self):
        from database import APPDATA
        if 'otts.txt' in os.listdir(f"{os.getcwd()}{os.sep}database"):
            os.remove(f"{os.getcwd()}{os.sep}database{os.sep}otts.txt")
        APPDATA.generate_otts(2)
        time.sleep(5)  # waiting for keys to be encrypted and written
        with open(f"{os.getcwd()}{os.sep}database{os.sep}otts.txt", "r") as f:
            new_keys = f.readlines()
        self.assertNotEqual(new_keys[0], new_keys[1])
        self.assertTrue(len(new_keys[1]) > 20)

    @create_test_env()
    @load_package(["database"])
    def test_ott_deletion_upon_regeneration(self):
        from database import APPDATA
        APPDATA.generate_otts(1)
        time.sleep(2)
        with open(f"{os.getcwd()}{os.sep}database{os.sep}otts.txt", "r") as f:
            old_keys = f.readlines()
        APPDATA.generate_otts(1)
        time.sleep(3)  # waiting for keys to be encrypted and written
        test_key = old_keys[0].strip()
        self.assertFalse(APPDATA.check_ott(test_key))
        APPDATA.db_session.close()

    @create_test_env()
    @load_package(["database"])
    def test_oot_check_not_present(self):
        from database import APPDATA
        APPDATA.generate_otts(1)
        time.sleep(2)
        test_key = "a"
        self.assertFalse(APPDATA.check_ott(test_key))
        APPDATA.db_session.close()

    @create_test_env()
    @load_package(["database"])
    def test_oot_check_present_and_used(self):
        from database import APPDATA
        APPDATA.generate_otts(2)
        time.sleep(5)
        with open(f"{os.getcwd()}{os.sep}database{os.sep}otts.txt", "r") as f:
            keys = f.readlines()
        test_key = keys[0].strip()
        self.assertTrue(APPDATA.check_ott(test_key))
        time.sleep(0.5)
        self.assertFalse(APPDATA.check_ott(test_key))
        APPDATA.db_session.close()


class TestOttFileDeletion(unittest.TestCase):

    @create_test_env()
    def test_delete_file_existing(self):
        import os
        from database import APPDATA
        with open(f"{os.getcwd()}{os.sep}database{os.sep}otts.txt", "w") as f:
            f.write("hi")
        APPDATA.delete_ott_file()
        self.assertFalse('otts.txt' in os.listdir(f"{os.getcwd()}{os.sep}database"))

    @create_test_env()
    def test_delete_file_missing(self):
        from database import APPDATA
        APPDATA.delete_ott_file()


if __name__ == '__main__':
    unittest.main()
